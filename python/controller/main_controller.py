import python.controller.validate_config as validate_config
import python.detector.detector as detector
import python.postprocess.view_results as view_result
import python.postprocess.save_image as save_image
import python.queue_manager.queue_manager as queue_manager
import python.postprocess.validate_coordinates as count
import python.preprocess.meta_data_read as meta_data_read
import python.preprocess.crop_image as crop_image

config_path = '/home/thisara/Desktop/generalized_machine_learning_architecture/python/configuration/config.json'
queue_manager_param, detection_param = validate_config.convert_dic(config_path)
metadata = meta_data_read.read_data(queue_manager_param.meta_data)

queue_manager.initiate_queue_manager()
queue_obj = queue_manager.initiate_queue(queue_manager_param.data_type, queue_manager_param.dir_path,
                                         queue_manager_param.save_path, queue_manager_param.processed_image_path,
                                         queue_manager_param.speed)

detector.initiate_framework(detection_param)
while True:
    if not queue_obj.queue.empty():
        image = queue_obj.queue.get()
        image_name = queue_obj.img_in_queue[0]
        del queue_obj.img_in_queue[0]
        save_image.save(image, image_name, queue_obj.processed_image_path)
        coordinate, crop = meta_data_read.get_data(queue_obj.meta_data_list[0], metadata)
        del queue_obj.meta_data_list[0]
        cropped_image = crop_image.crop(image, crop)
        for img in cropped_image:
            detection = detector.detection(detection_param, img)
            #view_result.view(img, detection)
            count_list, box_list = count.get_count(detection["0"][0], coordinate)
            print(count_list)
            for i in box_list:
                j= {"0": (i, 0)}
                view_result.view(img, j)
            save_image.save(img, image_name, queue_obj.save_path)
