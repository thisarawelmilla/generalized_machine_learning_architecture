import tensorflow as tf
import cv2
import numpy as np


class tensorflow_detection_net:
    def __init__(self, model_path):
        self.path_to_model = model_path
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.path_to_model, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        self.default_graph = self.detection_graph.as_default()
        self.sess = tf.Session(graph=self.detection_graph)

        self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

    def process_frame(self, image):
        image_np_expanded = np.expand_dims(image, axis=0)
        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded})
        return boxes, scores, classes, num

    def close(self):
        self.sess.close()
        self.default_graph.close()


def initiate_model(model_path):
    global net
    net = tensorflow_detection_net(model_path=model_path)


def detector(threshold, img, class_list="all_classes"):
    im_height, im_width = img.shape[:2]
    img = cv2.resize(img, (1060, 600))
    boxes, scores, classes, num = net.process_frame(img)
    detection = {}
    for class_inst in class_list:
        detection[class_inst] = ([], 0)

    for i in range(boxes.shape[1]):
        class_index = classes[0, i]
        if scores[0, i] > threshold and (class_list == "all_classes" or class_index in class_list):
            detection[class_index][0].append((int(boxes[0, i, 0] * im_height),
                                              int(boxes[0, i, 1] * im_width),
                                              int(boxes[0, i, 2] * im_height),
                                              int(boxes[0, i, 3] * im_width)))
    return detection
