import ast
import json


def read_data(data_path):
    with open(data_path) as config_file:
        config_data = json.load(config_file)
    return config_data


def get_data(cam_id_in_byte, config_data):
    cam_id = cam_id_in_byte.decode("utf-8")
    coordinates = ast.literal_eval(config_data[cam_id]["coordinates"])
    crop = ast.literal_eval(config_data[cam_id]["crop"])
    return coordinates, crop
