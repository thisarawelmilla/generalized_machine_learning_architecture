import matplotlib.pyplot as plt
import cv2



def view(img, detection):
    for i in detection:
        for box in detection[i][0]:
            cv2.rectangle(img, (box[1], box[0]), (box[3], box[2]), (255, 0, 0), 2)
        cv2.rectangle(img, (120, 70), (480, 270), (0, 0, 255), 2)
    #cv2.imshow("preview", img)
    #cv2.waitKey(2000)
    plt.imshow(img, interpolation='none') # Plot the image, turn off interpolation
    plt.show()

