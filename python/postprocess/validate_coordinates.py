def get_out_range(out_line, inner_line):
    vol = inner_line - out_line
    if vol < 0:
        vol = 0
    return vol


def get_total_vol(box, coordinate):
    width = box[2] - box[0]
    height = box[3] - box[1]
    upper_out = get_out_range(box[0], coordinate[0])
    left_out = get_out_range(box[1], coordinate[1])
    lower_out = get_out_range(coordinate[2], box[2])
    right_out = get_out_range(coordinate[3], box[3])
    upper = upper_out * width
    lower = lower_out * width
    modified_height = (height - upper_out - lower_out)
    left = left_out * modified_height
    right = right_out * modified_height
    total = upper + lower + left + right
    return total


def get_count(boxes_list, coord_list):
    count_list = []
    all_boxes = []
    for coordinate in coord_list:
        count = 0
        box_list = []
        for box in boxes_list:
            total_out_vol = get_total_vol(box, coordinate)
            total_vol = (box[2] - box[0]) * (box[3] - box[1])
            prob = (total_vol - total_out_vol) / total_vol
            if prob > 0.9:
                count += 1
                box_list.append(box)
        count_list.append(count)
        all_boxes.append(box_list)
    return count_list, all_boxes



