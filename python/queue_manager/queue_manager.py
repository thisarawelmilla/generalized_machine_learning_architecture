from concurrent.futures import ThreadPoolExecutor
import queue as Queue
import time
import os
import cv2
from iptcinfo3 import IPTCInfo as iptc_info

queue_list = []


class QueueObject(object):
    def __init__(self, data_type, dir_path, save_path, processed_image_path, speed):
        self.dir_path = dir_path
        self.save_path = save_path
        self.processed_image_path = processed_image_path
        self.data_type = data_type
        self.speed = speed
        self.queue = Queue.Queue(3)
        self.lock = False
        self.img_list = os.listdir(self.dir_path)
        self.img_in_queue = []
        self.meta_data_list = []

    def get_datatype(self):
        return self.data_type

    def get_queue(self):
        return self.queue

    def get_lock(self):
        return self.lock

    def set_lock(self, status):
        self.lock = status

    def add_images(self):
        img_list = os.listdir(self.dir_path)
        self.img_list = img_list

    def read_meta_data(self, image_name):
        info = iptc_info(self.dir_path + "/" + image_name)
        cam_id = info['content location code']
        self.meta_data_list.append(cam_id)


def image_queue(q):
    if len(q.img_list) == 0:
        q.add_images()
    while not q.get_lock() and len(q.img_list) > 0:
        q.set_lock(True)
        image_name = q.img_list[0]
        image = cv2.imread(q.dir_path + "/" + image_name)
        q.read_meta_data(image_name)
        del q.img_list[0]
        os.remove(q.dir_path + "/" + image_name)
        q.get_queue().put(image)
        q.img_in_queue.append(image_name)
        q.set_lock(False)
        break


def check_queue():
    while True:
        if len(queue_list) != 0:
            for q in queue_list:
                if q.get_queue().qsize() < 3:
                    if q.get_datatype() == "image":
                        image_queue(q)
                if q.get_queue().qsize() >= 3:
                    time.sleep(1)


# make a thread
def initiate_queue_manager():
    global executor
    executor = ThreadPoolExecutor(1)


def initiate_queue(data_type, dir_path, save_path, processed_image_path, speed):
    new_queue = QueueObject(data_type, dir_path, save_path, processed_image_path, speed)
    queue_list.append(new_queue)
    time.sleep(1)
    if len(queue_list) != 0:
        executor.submit(check_queue)
    return new_queue
